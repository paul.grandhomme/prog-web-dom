<!DOCTYPE html>
<html>
  <head>
    <title>Calendrier</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="style_calendrier.css">
  </head>
  <body>
    

    <?php
    $date = $_GET['date'] ?? "01/01/2022";
    $newDate = new DateTime($date);
    echo "Create event :";

    $month = mb_substr($newDate->format('Y-m-d'), 5, 2) ?? 1;
    $year = mb_substr($newDate->format('Y-m-d'), 0, 4) ?? 2022;
    $event = mb_substr($newDate->format('Y-m-d'), 8, 2) ?? 0;
    $desc = $_GET['description'] ?? '';
    
    $maxday=31;
    if($month==4 || $month==4 || $month==6 || $month==9 || $month==11) $maxday=30;
    if($month==2) $maxday=28;
    
    for($i=1; $i<=7; $i++){
      switch(date("l", mktime(0, 0, 0, $month, $i, $year))){
        case "Monday" :
          $firstmon = $i;
          break;
        case "Tuesday" :
          $firsttue = $i;
          break;
        case "Wednesday" :
          $firstwed = $i;
          break;
        case "Thursday" :
          $firstthu = $i;
          break;
        case "Friday" :
          $firstfri = $i;
          break;
        case "Saturday" :
          $firstsat = $i;
          break;
        case "Sunday" :
          $firstsun = $i;
          break;
      }
    }

    switch($month){
      case '01' :
        $monthname="January";
        break;
      case '02' :
        $monthname="February";
        break;
      case '03' :
        $monthname="March";
        break;
      case '04' :
        $monthname="April";
        break;
      case '05' :
        $monthname="May";
        break;
      case '06' :
        $monthname="June";
        break;
      case '07' :
        $monthname="July";
        break;
      case '08' :
        $monthname="August";
        break;
      case '09' :
        $monthname="September";
        break;
      case '10' :
        $monthname="October";
        break;
      case '11' :
        $monthname="November";
        break;
      case '12' :
        $monthname="December";
        break;
    }

    ?>

    <form method="get" action="calendrier.php">
      <input type="date" id="date" name="date">
      <input type="texte" id="description" name="description">
      <input type="submit"/>
    </form>

    <label><?php echo $monthname." "; echo $year; ?></label>

    <ol>
      <li class="numero" title="Monday">
        <?php
          echo "<p class='day'>Monday</p>";
          if($firstmon>1) echo "<p>_</p>";
          for($num=$firstmon; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Tuesday">
        <?php
          echo "<p class='day'>Tuesday</p>";
          if($firsttue>2) echo "<p>_</p>";
          for($num=$firsttue; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Wednesday">
        <?php
          echo "<p class='day'>Wednesday</p>";
          if($firstwed>3) echo "<p>_</p>";
          for($num=$firstwed; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Thursday">
        <?php
          echo "<p class='day'>Thursday</p>";
          if($firstthu>4) echo "<p>_</p>";
          for($num=$firstthu; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Friday">
        <?php
          echo "<p class='day'>Friday</p>";
          if($firstfri>5) echo "<p>_</p>";
          for($num=$firstfri; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Saturday">
        <?php
          echo "<p class='day'>Saturday</p>";
          if($firstsat>6) echo "<p>_</p>";
          for($num=$firstsat; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
      <li class="numero" title="Sunday">
        <?php
          echo "<p class='day'>Sunday</p>";
          for($num=$firstsun; $num<=$maxday; $num+=7){
            echo "<p>".$num;
            if($event==$num){
              echo "<d>$desc</d>";
            }
            echo "</p>";
          }
        ?>
      </li>
    </ol>
  </body>
</html>
