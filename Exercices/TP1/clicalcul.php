<?php

require_once 'libcalcul.php';

$tab = ['Somme' => $argv[1], 'Taux' => $argv[2], 'Duree' => $argv[3]];

echo $tab['Somme']." * ( 1 + ".$tab['Taux']."/100 ) ^ ".$tab['Duree'];
echo " = ".cumul($tab['Somme'], $tab['Taux'], $tab['Duree'])."\n";
