<!DOCTYPE html>
<html>
<head>
<title>Multiplications</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="table.css">
</head>

<?php
  $n = $_GET["n"] ??10;
?>

<body>
	<table>
  <thead>
    <tr>
      <th>X</th>
      <?php
      	for ($i = 1; $i <= $n; $i++) {
          echo "<th>".$i."</th>";
        }
      ?>
    </tr>
  </thead>
  <tbody>
      <?php
        for ($i = 1; $i <= $n; $i++) {
          echo "<tr><th>".$i."</th>";
          for ($y = 1; $y <= $n; $y++) {
            echo "<td>".$i*$y."</td>";
          }
          echo "</tr>";
        }
      ?>
  </tbody>
  </table>
</body>
</html>
