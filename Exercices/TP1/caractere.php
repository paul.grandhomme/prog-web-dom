<!DOCTYPE html>
<html>
  <head>
    <title>Caractere</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <?php
    $txt = $_GET['texte'] ??"";
    ?>
<!-- method peut prendre les valeurs "get" et "post" -->
    <form method="get" action="caractere.php">
    <label for="texte">Texte : </label> <input type="text" id="texte" name="texte" value="<?php echo $txt?>"/>
    <input type="submit"/>
    </form>

    <ol class="unicode">
      <?php
        $txt = $_GET['texte'] ??"";
        $charac = mb_substr($txt, 0, 1);
        if(mb_strlen($charac)==0){
          $var = 0;
        }else {
          $var = mb_ord($charac);
        }
        $num = $var-$var%16;
        for ($y = $num; $y <= $num+15; $y++) {
            $output = null;
            exec("unicode -x '" . dechex($y) . "'", $output);
            $title= explode('UTF-8', implode('', $output))[0];
            echo '<li title="' . $title . '"';
            if($y==$var) {
              echo " type=\"yell\"";
            }else{
              echo " type=\"whi\"";
            }
            echo ">";
            echo "<p>";
            printf('%c', $y);
            echo "</p>";
            echo "<a href=\"https://util.unicode.org/UnicodeJsps/character.jsp?a=";
            printf('%04X', $y);
            echo "\">";
            printf('U+%04X', $y);
            echo "</a>";
            echo "</li>";
        }
      ?>
    </ol>

  </body>
</html>
