<?php

require_once 'libcalcul.php';

$tab = ['Somme' => $_GET['Somme'], 'Taux' => $_GET['Taux'], 'Duree' => $_GET['Duree']];

echo $tab['Somme']." * ( 1 + ".$tab['Taux']."/100 ) ^ ".$tab['Duree'];
echo " = ".cumul($tab['Somme'], $tab['Taux'], $tab['Duree'])."\n";
