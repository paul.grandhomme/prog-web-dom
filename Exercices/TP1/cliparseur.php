<?php

//with DomDoc
$dom = new DOMDocument;
$dom->loadHTMLFile($argv[1]);
$h2 = $dom->getElementsByTagName('h2');
echo "h2 :";
printf("\n");
foreach ($h2 as $line2) {
    echo $line2->textContent;
    printf("\n");
}
$h3 = $dom->getElementsByTagName('h3');
echo "h3 :";
printf("\n");
foreach ($h3 as $line3) {
    echo $line3->textContent;
    printf("\n");
}

printf("\n\n");
//without DomDoc
preg_match('/(?)(<h2>)(?)(<\/h2>)(?)/', $argv[1], $matches, PREG_UNMATCHED_AS_NULL);
var_dump($matches);