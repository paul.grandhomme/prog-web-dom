<!DOCTYPE html>
<html>
  <head>
    <title>TMDB - Movie Description</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="TMDB_style.css">
  </head>
  <body>
  	<?php

require_once'tp3-helpers.php';

function getLink($json){
	return "https://www.themoviedb.org/movie/".$json['id'];
}
$identifiant = 119;
$json_collection = json_decode(tmdbget("collection/".$identifiant), true);

$tableau_json = $json_collection['parts'];
?>

    <table>
    	<tr><th class="table_row">Title : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", $value['title']);?></tr>
    	<tr><th class="table_row">Identifiant : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>",$value['id']);?></tr>
    	<tr><th class="table_row">Sortie : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", $value['release_date']);?></tr>
    	<tr><th class="table_row">Poster : </th><?php foreach($tableau_json as $key => $value) printf("<td><img src=\"https://image.tmdb.org/t/p/w185/%s\"></td>", $value['poster_path']);?></tr>
    	<tr><th class="table_row">Link : </th><?php foreach($tableau_json as $key => $value) printf("<td><a href=\"%s\">%s</a></td>", getLink($value), getLink($value));?></tr>
		</table>
  </body>
</html>
