<!DOCTYPE html>
<html>
  <head>
    <title>TMDB - Movie Description</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="TMDB_style.css">
  </head>
  <body>
  	<?php

require_once'tp3-helpers.php';

function getTitle($json){
	return $json['title'];
}

function getOriginalTitle($json){
	return $json['original_title'];
}

function getTagline($json){
	return $json['tagline'];
}

function getDescription($json){
	return $json['overview'];
}

function getLink($json){
	return "https://www.themoviedb.org/movie/".$json['id'];
}

$identifiant = $_GET['ident'] ?? 550;
if(json_decode(tmdbget("movie/".$identifiant), true)==NULL)$identifiant = 550;
$json_vo = json_decode(tmdbget("movie/".$identifiant), true);
$json_eng = json_decode(tmdbget("movie/".$identifiant, ['language' => 'eng']), true);
$json_fr = json_decode(tmdbget("movie/".$identifiant, ['language' => 'fr']), true);

$tableau_json = array($json_vo, $json_eng, $json_fr);
$tableau_lang = array("ov", "eng", "fr");

printf("Identifiant du film (550 par défaut)");
?>
    <form method="get" action="TMDB_infos_film.php">
      <input type="text" id="ident" name="ident">
      <input type="submit"/>
    </form>
    <table>
    	<tr><th class="table_row">Language</th><th>Original</th><th>English</th><th>Français</th></tr>
    	<tr><th class="table_row">Title : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", getTitle($value));?></tr>
    	<tr><th class="table_row">Original title : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", getOriginalTitle($value));?></tr>
    	<tr><th class="table_row">Tagline : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", getTagline($value));?></tr>
    	<tr><th class="table_row">Description : </th><?php foreach($tableau_json as $key => $value) printf("<td>%s</td>", getDescription($value));?></tr>
    	<tr><th class="table_row">Poster : </th><?php foreach($tableau_json as $key => $value) printf("<td><img src=\"https://image.tmdb.org/t/p/w185/%s\"></td>", $value['poster_path']);?></tr>
    	<tr><th class="table_row">Link : </th><?php foreach($tableau_json as $key => $value) printf("<td><a href=\"%s?language=%s\">%s?language=%s</a></td>", getLink($value), $tableau_lang[$key], getLink($value), $tableau_lang[$key]);?></tr>
		</table>
  </body>
</html>
