% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : 

## Participants 

* Un
* Deux
* ...

Les organismes participant à ce site sont :

- ANFR
- Centre Communal d'Action Sociale de Grenoble
- Etalab
- Ville de Grenoble
- Grenoble Alpes Métropole
- INSEE
- L'Agence d'Urbanisme de la Région Grenobloise
- SMMAG
- Ville d'Echirolles
- Ville de Fontaine
- Ville de Meylan
- Ville de Pont de Claix
- Ville de Saint Egrève
- Ville de Saint Martin d'Hères
- Ville d'Eybens

## Points d'accès Wifi

### Filtres UNIX

On peut compter le nombre de bornes avec une de ces commandes :

```bash
# Avec le CSV et les outils UNIX classiques
cat Donnees/borneswifi_EPSG4326_20171004_utf8.csv | tail -n +2 | cut -d , -f 1 | sort | uniq | wc -l
# Ou, avec les données en JSON et jq
cat Donnees/borneswifi_EPSG4326_20171004.json | jq .features[].properties.AP_ANTENNE1 | sort | uniq | wc -l
```

On en trouve 68 différents.

Si on les groupe, on en a que 58. L'endroit où il y a le plus de point d'accès est la bibliothèque d'études avec 5 réseaux. On utilise pour celà cette commande : 

```bash
cat Donnees/borneswifi_EPSG4326_20171004_utf8.csv | tail -n +2 | cut -d , -f 2 | sort | uniq -c | sort -nrb
```

## Antennes GSM

Il y a 100 antennes GSM dans Grenoble.

Ce jeu de données indique pour chaque antenne :

- un identifiant unique ;
- l'opérateur à qui elle appartient ;
- les technologies (2G, 3G, 4G) supportées ;
- une adresse lisible par des humains ;
- quelques autres champs que nous ne comprenons pas.

Avoir ces champs directement dans le jeu de données permet de les exploiter plus facilement.
Par exemple, avoir déjà une adresse lisible par des humains évite de devoir recourir à un service
de géocodage inverse comme dans la partie précédente.

On peut facilement valider les fichiers KML car l'en-tête XML a référence un schéma avec l'attribut `xmlns`.

Ce fichier est facile à exploiter avec une machine, mais est assez long et redondant pour un humain qui voudrait le lire à la main.
