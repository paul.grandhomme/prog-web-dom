<?php

require_once'tp2-helpers.php';

$tab=array();
$fichier = fopen($argv[1], "r");
$index=0;
$data = fgetcsv($fichier);
while(($data = fgetcsv($fichier))!=NULL){
	$ligne=array();
	$ligne['name']=$data[0];
	$ligne['adr']=$data[1];
	$ligne['lon']=(float)$data[2];
	$ligne['lat']=(float)$data[3];
	$tab[$index]=$ligne;
	$index++;
}

$mylat=45.19102;
$mylon=5.72752;
$mypos=geopoint($mylon, $mylat);

$N=5;
if($argc>=3)$N=(int)$argv[2];
if($N>count($tab))$N=count($tab);
$distances=array();
$indexes=array();
$index=0;
foreach($tab as $wifi){
	$distances[$index]=distance($mypos, geopoint($wifi['lon'], $wifi['lat']));
	$indexes[$index]=$index;
	$index++;
}
array_multisort($distances, $indexes);

printf("Les %d bornes wifi les plus proches sont :\n", $N);
for($i=0; $i<$N; $i++){
	printf("%d. %s : %fm\n", $i+1, $tab[$indexes[$i]]['name'], $distances[$i]);
}