<?php
$top = $_GET['top'] ?? 5;
$lon = $_GET['lon'] ?? 5.72752;
$lat = $_GET['lat'] ?? 45.19102;
$tab=array();
$fichier = file_get_contents("../../Donnees/borneswifi_EPSG4326_20171004.json", true);
$index=0;
$json=json_decode($fichier, true);

while($index<count($json['features'])){
    $ligne=array();
    $ligne['name']=$json['features'][$index]['properties']['AP_ANTENNE1'];
    $ligne['adr']=$json['features'][$index]['properties']['Antenne 1'];
    $ligne['lon']=(float)$json['features'][$index]['geometry']['coordinates'][0];
    $ligne['lat']=(float)$json['features'][$index]['geometry']['coordinates'][1];
    $tab[$index]=$ligne;
    $index++;
}

$mypos=geopoint($lon, $lat);

if ($top>count($tab)) {
    $top=count($tab);
}

$distances=array();
$index=0;
foreach($tab as $wifi){
    $distances[$index]=distance($mypos, geopoint($wifi['lon'], $wifi['lat']));
    $index++;
}
array_multisort($distances, $tab);

echo json_encode(array_slice($tab, 0, $top));

/**
 * @param float $lon
 * @param float $lat
 * @return array ['lon'=>float, 'lat'=>float]
 */
function geopoint($lon, $lat) {
  return ['lon'=>$lon, 'lat'=>$lat];
}

/* 
   @param $p array('lon'=>int, 'lat'=>int)
   @param $q array('lon'=>int, 'lat'=>int)
   @return int distance (approx.) in meters
*/
function distance ($p, $q) {
  $scale = 10000000 / 90; // longueur d'un degré le long d'un méridien
  $a = ((float)$p['lon'] - (float)$q['lon']);
  $b = (cos((float)$p['lat']/180.0*M_PI) * ((float)$p['lat'] - (float)$q['lat']));
  $res = $scale * sqrt( $a**2 + $b**2 );
  return (float)sprintf("%5.1f", $res);
}
?>
