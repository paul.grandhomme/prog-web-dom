<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf8">
        <title>La distance des réseaux</title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    </head>
    <body>
        <div id="map"></div>

        <aside>
            <p><strong>Position sélectionnée : </strong><span id="pos"></span></p>
            <ol id="bornes">

            </ol>
        </aside>

        <script>
let pos = [45.19102, 5.72752]
const posSpan = document.getElementById('pos')
const map = L.map('map', {
    center: pos,
    zoom: 13
})
let marker = L.marker(pos).addTo(map)
const list = document.getElementById('bornes')
let markers = {}

const updatePos = async () => {
    posSpan.textContent = `${pos[0].toFixed(5)} ${pos[1].toFixed(5)}`
    marker.setLatLng(pos)
    const req = await fetch(`Bornes.php?lat=${pos[0]}&lon=${pos[1]}`)
    const bornes = await req.json()
    list.childNodes.forEach(x => list.removeChild(x))
    for (const b of bornes) {
        const li = document.createElement('li')
        if (markers[b.name] == undefined) {
            markers[b.name] = L.marker([b.lat, b.lon])
        }
        li.textContent = b.name
        list.appendChild(li)
        li.addEventListener('mouseenter', _ => {
            markers[b.name].addTo(map)
        })
        li.addEventListener('mouseleave', _ => {
            markers[b.name].remove()
        })
    }
}

updatePos()

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map)

map.on('click', evt => {
    pos = [evt.latlng.lat, evt.latlng.lng]
    updatePos()
})
        </script>
    </body>
</html>
