<?php

$lon = 5.72752;
$lat = 45.19102;
if($argc>=3){
	$lon=$argv[1];
	$lat=$argv[2];
}else{
	printf("Try using : php Geocodage.py <lon> <lat>\nDefault : lon=5.72752 lat=45.19102\n\n");
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, "https://api-adresse.data.gouv.fr/reverse/?lon=".$lon."&lat=".$lat);
$result = curl_exec($ch);
$json=json_decode($result, true);
printf("Result : %s\n", $json['features'][0]['properties']['label']);
curl_close($ch);

