<?php

function csv_bool($str) {
    return $str == "OUI";
}

function gsm_object($data) {
    return array(
        "ANT_ID" => (int) $data[0],
        "ADRES_ID" => (int) $data[1],
        "MICROCELL" => csv_bool($data[2]),
        "OPERATEUR" => $data[3],
        "ANT_TECHNO" => $data[4],
        "X" => (int) $data[5],
        "Y" => (int) $data[6],
        "ANT_ADRES_LIBEL" => $data[7],
        "NUM_CARTORADIO" => (int) $data[8],
        "NUM_SUPPORT" => (int) $data[9],
        "ANT_2G" => csv_bool($data[10]),
        "ANT_3G" => csv_bool($data[11]),
        "ANT_4G" => csv_bool($data[12]),
    );
}

$csv = fopen("Donnees/DSPE_ANT_GSM_EPSG4326.csv", "r");
$compteur = 0;
$ops = array();
fgetcsv($csv); // skip the first line
while (($data = fgetcsv($csv, null, ";"))) {
    $gsm = gsm_object($data);
    $compteur += 1;
    if (!in_array($gsm["OPERATEUR"], $ops)) {
        array_push($ops, $gsm["OPERATEUR"]);
    }
}

printf("Il y a %d antennes GSM.\n", $compteur);
printf("Il y a %d opérateurs différents :\n", count($ops));
foreach ($ops as $op) {
    printf("- %s\n", $op);
}

fclose($csv);

?>
